#include "Physics.h"

#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"
#include "Gizmos.h"
#include <iostream>

#include "glm/ext.hpp"
#include "glm/gtc/quaternion.hpp"
#include "physx/cooking/PxCooking.h"

#define Assert(val) if (val){}else{ *((char*)0) = 0;}
#define ArrayCount(val) (sizeof(val)/sizeof(val[0]))

// Misc
static RagdollNode* ragdollData[] = {
	new RagdollNode(PxQuat(PxPi / 2.0f, Z_AXIS), NO_PARENT, 1,3,1,1,"lower spine"),
	new RagdollNode(PxQuat(PxPi, Z_AXIS), LOWER_SPINE, 1,1,-1,1,"left pelvis"),
	new RagdollNode(PxQuat(0, Z_AXIS), LOWER_SPINE, 1,1,-1,1,"right_pelvis"),
	new RagdollNode(PxQuat(PxPi / 2.0f + 0.2f, Z_AXIS),LEFT_PELVIS,5,2,-1,1,"L upper leg"),
	new RagdollNode(PxQuat(PxPi / 2.0f - 0.2f, Z_AXIS),RIGHT_PELVIS,5,2,-1,1,"R upper leg"),
	new RagdollNode(PxQuat(PxPi / 2.0f + 0.2f, Z_AXIS),LEFT_UPPER_LEG,5,1.75,-1,1,"L lower leg"),
	new RagdollNode(PxQuat(PxPi / 2.0f - 0.2f, Z_AXIS),RIGHT_UPPER_LEG,5,1.75,-1,1,"R lowerleg"),
	new RagdollNode(PxQuat(PxPi / 2.0f, Z_AXIS), LOWER_SPINE, 1, 3, 1, -1, "upper spine"),
	new RagdollNode(PxQuat(PxPi, Z_AXIS), UPPER_SPINE, 1, 1.5, 1, 1, "left clavicle"),
	new RagdollNode(PxQuat(0, Z_AXIS), UPPER_SPINE, 1, 1.5, 1, 1, "right clavicle"),
	new RagdollNode(PxQuat(PxPi / 2.0f, Z_AXIS), UPPER_SPINE, 1, 1, 1, -1, "neck"),
	new RagdollNode(PxQuat(PxPi / 2.0f, Z_AXIS), NECK, 1, 3, 1, -1, "HEAD"),
	new RagdollNode(PxQuat(PxPi - .3f, Z_AXIS), LEFT_CLAVICLE, 3, 1.5, -1, 1, "left upper arm"),
	new RagdollNode(PxQuat(0.3f, Z_AXIS), RIGHT_CLAVICLE, 3, 1.5, -1, 1, "right upper arm"),
	new RagdollNode(PxQuat(PxPi - .3f, Z_AXIS), LEFT_UPPER_ARM, 3, 1, -1, 1, "left lower arm"),
	new RagdollNode(PxQuat(0.3f, Z_AXIS), RIGHT_UPPER_ARM, 3, 1, -1, 1, "right lower arm"),
	NULL
};
void setupFiltering(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask) {
	PxFilterData filterData;
	filterData.word0 = filterGroup;
	filterData.word1 = filterMask;

	const PxU32 numShapes = actor->getNbShapes();
	PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*)*numShapes, 16);
	actor->getShapes(shapes, numShapes);
	for (PxU32 i = 0; i < numShapes; i++) {
		PxShape* shape = shapes[i];
		shape->setSimulationFilterData(filterData);
	}
	_aligned_free(shapes);
}
void setShapeAsTrigger(PxRigidActor* actorIn) {
	PxRigidStatic* staticActor = actorIn->is<PxRigidStatic>();
	assert(staticActor);

	const PxU32 numShapes = staticActor->getNbShapes();
	PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*)*numShapes, 16);
	staticActor->getShapes(shapes, numShapes);
	for (PxU32 i = 0; i < numShapes; i++) {
		shapes[i]->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
		shapes[i]->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
	}
}

// RUN
bool Physics::startup()
{
    if (Application::startup() == false)
    {
        return false;
    }
	
    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    Gizmos::create();

    m_camera = FlyCamera(1280.0f / 720.0f, 10.0f);
    m_camera.setLookAt(vec3(10, 10, 10), vec3(0), vec3(0, 1, 0));
    m_camera.sensitivity = 3;

	m_renderer = new Renderer();
	setupPhysX();

	m_ragdollData = ragdollData;

	pool();
	Controller();
	playGround();
	ragdollScene();
	Trigger();

    return true;
}
void Physics::shutdown()
{
	delete m_renderer;
	m_physics_scene->release();
	m_physics_foundation->release();
	m_physics->release();
    Gizmos::destroy();
    Application::shutdown();
}
bool Physics::update()
{
    if (Application::update() == false)
    {
        return false;
    }

    Gizmos::clear();

    float dt = (float)glfwGetTime();
    m_delta_time = dt;
    glfwSetTime(0.0);

    vec4 white(1);
    vec4 black(0, 0, 0, 1);

    for (int i = 0; i <= 20; ++i)
    {
        Gizmos::addLine(vec3(-10 + i, -0.01, -10), vec3(-10 + i, -0.01, 10),
            i == 10 ? white : black);
        Gizmos::addLine(vec3(-10, -0.01, -10 + i), vec3(10, -0.01, -10 + i),
            i == 10 ? white : black);
    }

    m_camera.update(1.0f / 60.0f);

	vec3 cam_pos = m_camera.world[3].xyz();
	vec3 box_vel = -m_camera.world[2].xyz() * 20.0f;

	PxTransform box_transform(PxVec3(cam_pos.x, cam_pos.y, cam_pos.z));
	PxBoxGeometry box(0.5f, 0.5f, 0.5f);

	PxRigidDynamic* new_actor = PxCreateDynamic(*m_physics, box_transform, box, *m_physics_material, 5);

	if (counter > 0) {
		if (glfwGetKey(m_window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			counter -= 1;
			glm::vec3 direction(-m_camera.world[2]);
			physx::PxVec3 velocity = physx::PxVec3(direction.x, direction.y, direction.z) * 100;
			new_actor->setLinearVelocity(velocity, true);
			m_physics_scene->addActor(*new_actor);
		}
	}

	triggerUpdate();

	if (m_particleEmitter) {
		m_particleEmitter->update(m_delta_time);
		m_particleEmitter->renderParticles();
	}

	renderGizmos(m_physics_scene);
	movementController(m_delta_time);

	if (dt > 0) {
		m_physics_scene->simulate(dt > 0.033f ? 0.033f : dt);
		while (m_physics_scene->fetchResults() == false);
	}

    return true;
}
void Physics::draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_CULL_FACE);
    Gizmos::draw(m_camera.proj, m_camera.view);

    m_renderer->RenderAndClear(m_camera.view_proj);

    glfwSwapBuffers(m_window);
    glfwPollEvents();
}
void Physics::AddWidget(PxShape* shape, PxRigidActor* actor, vec4 geo_color)
{
    PxTransform full_transform = PxShapeExt::getGlobalPose(*shape, *actor);
    vec3 actor_position(full_transform.p.x, full_transform.p.y, full_transform.p.z);
    glm::quat actor_rotation(full_transform.q.w,
        full_transform.q.x,
        full_transform.q.y,
        full_transform.q.z);
    glm::mat4 rot(actor_rotation);

    mat4 rotate_matrix = glm::rotate(10.f, glm::vec3(7, 7, 7));

    PxGeometryType::Enum geo_type = shape->getGeometryType();

    switch (geo_type)
    {
    case (PxGeometryType::eBOX) :
    {
        PxBoxGeometry geo;
        shape->getBoxGeometry(geo);
        vec3 extents(geo.halfExtents.x, geo.halfExtents.y, geo.halfExtents.z);
        Gizmos::addAABBFilled(actor_position, extents, geo_color, &rot);
    } break;
    case (PxGeometryType::eCAPSULE) :
    {
        PxCapsuleGeometry geo;
        shape->getCapsuleGeometry(geo);
        Gizmos::addCapsule(actor_position, geo.halfHeight * 2, geo.radius, 8, 8, geo_color, &rot);
    } break;
    case (PxGeometryType::eSPHERE) :
    {
        PxSphereGeometry geo;
        shape->getSphereGeometry(geo);
        Gizmos::addSphereFilled(actor_position, geo.radius, 8, 8, geo_color, &rot);
    } break;
    case (PxGeometryType::ePLANE) :
    {

    } break;
    }
}
void Physics::renderGizmos(PxScene* physics_scene)
{
    PxActorTypeFlags desiredTypes = PxActorTypeFlag::eRIGID_STATIC | PxActorTypeFlag::eRIGID_DYNAMIC;
    PxU32 actor_count = physics_scene->getNbActors(desiredTypes);
    PxActor** actor_list = new PxActor*[actor_count];
	physics_scene->getActors(desiredTypes, actor_list, actor_count);
    
    vec4 geo_color(1, 0, 0, 0.2);
    for (int actor_index = 0;
        actor_index < (int)actor_count;
        ++actor_index)
    {
        PxActor* curr_actor = actor_list[actor_index];
        if (curr_actor->isRigidActor())
        {
            PxRigidActor* rigid_actor = (PxRigidActor*)curr_actor;
            PxU32 shape_count = rigid_actor->getNbShapes();
            PxShape** shapes = new PxShape*[shape_count];
            rigid_actor->getShapes(shapes, shape_count);

            for (int shape_index = 0;
                shape_index < (int)shape_count;
                ++shape_index)
            {
                PxShape* curr_shape = shapes[shape_index];
                AddWidget(curr_shape, rigid_actor, geo_color);
            }

            delete[]shapes;
        }
    }

    delete[] actor_list;

    int articulation_count = physics_scene->getNbArticulations();

    for (int a = 0; a < articulation_count; ++a)
    {
        PxArticulation* articulation;
		physics_scene->getArticulations(&articulation, 1, a);

        int link_count = articulation->getNbLinks();

        PxArticulationLink** links = new PxArticulationLink*[link_count];
        articulation->getLinks(links, link_count);

        for (int l = 0; l < link_count; ++l)
        {
            PxArticulationLink* link = links[l];
            int shape_count = link->getNbShapes();

            for (int s = 0; s < shape_count; ++s)
            {
                PxShape* shape;
                link->getShapes(&shape, 1, s);
                AddWidget(shape, link, geo_color);
            }
        }
        delete[] links;
    }
}
void Physics::setupPhysX() {
	m_default_filter_shader = PxDefaultSimulationFilterShader;
	m_physics_foundation = PxCreateFoundation(PX_PHYSICS_VERSION, m_default_allocator, m_default_error_callback);
	m_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_physics_foundation, PxTolerancesScale());
	PxInitExtensions(*m_physics);
	m_physics_material = m_physics->createMaterial(1, 1, 0);
	m_physics_cooker = PxCreateCooking(PX_PHYSICS_VERSION, *m_physics_foundation, PxCookingParams(PxTolerancesScale()));

	m_physics_scene = createDefaultScene();
	mycollisionCallBack = new MyCollisionCallback();
	m_physics_scene->setSimulationEventCallback((PxSimulationEventCallback*)mycollisionCallBack);
}
PxScene* Physics::createDefaultScene() {
	PxSceneDesc scene_desc(m_physics->getTolerancesScale());
	scene_desc.gravity = PxVec3(0, -9.807f, 0);
	scene_desc.filterShader = myFilterShader;
	scene_desc.cpuDispatcher = PxDefaultCpuDispatcherCreate(8);
	PxScene* result = m_physics->createScene(scene_desc);
	return result;
}
PxArticulation* Physics::makeRagdoll(PxPhysics* g_Physics, RagdollNode** nodeArray,
	PxTransform worldPos, float scaleFactor, PxMaterial* ragdollMaterial) {
	PxArticulation* articulation = g_Physics->createArticulation();
	RagdollNode** currentNode = nodeArray;
	while (*currentNode != NULL) {
		RagdollNode* currentNodePtr = *currentNode;
		RagdollNode* parentNode = nullptr;

		float radius = currentNodePtr->radius*scaleFactor;
		float halfLength = currentNodePtr->halfLength*scaleFactor;
		float childHalfLength = radius + halfLength;
		float parentHalfLength = 0;

		PxArticulationLink* parentLinkPtr = NULL;
		currentNodePtr->scaledGlobalPos = worldPos.p;

		if (currentNodePtr->parentNodeIdx != -1) {
			parentNode = *(nodeArray + currentNodePtr->parentNodeIdx);
			parentLinkPtr = parentNode->linkPtr;
			parentHalfLength = (parentNode->radius + parentNode->halfLength) *scaleFactor;
			PxVec3 currentRelative = currentNodePtr->childLinkPos * currentNodePtr->globalRotation.rotate(PxVec3(childHalfLength, 0, 0));
			PxVec3 parentRelative = -currentNodePtr->parentLinkPos * parentNode->globalRotation.rotate(PxVec3(parentHalfLength, 0, 0));
			currentNodePtr->scaledGlobalPos = parentNode->scaledGlobalPos - (parentRelative + currentRelative);
		}

		PxTransform linkTransform = PxTransform(currentNodePtr->scaledGlobalPos, currentNodePtr->globalRotation);
		PxArticulationLink* link = articulation->createLink(parentLinkPtr, linkTransform);

		currentNodePtr->linkPtr = link;
		float jointSpace = 0.1f;
		float capsuleHalfLength = (halfLength > jointSpace ? halfLength - jointSpace : 0) + 0.01f;
		PxCapsuleGeometry capsule(radius, capsuleHalfLength);
		link->createShape(capsule, *ragdollMaterial);
		PxRigidBodyExt::updateMassAndInertia(*link, 50.0f);

		if (currentNodePtr->parentNodeIdx != -1) {
			PxArticulationJoint *joint = link->getInboundJoint();
			PxQuat frameRotation = parentNode->globalRotation.getConjugate() * currentNodePtr->globalRotation;
			PxTransform parentConstraintFrame = PxTransform(PxVec3(currentNodePtr->parentLinkPos * parentHalfLength, 0, 0), frameRotation);
			PxTransform thisConstraintFrame = PxTransform(PxVec3(currentNodePtr->childLinkPos * childHalfLength, 0, 0));

			joint->setParentPose(parentConstraintFrame);
			joint->setChildPose(thisConstraintFrame);

			joint->setStiffness(20);
			joint->setDamping(20);
			joint->setSwingLimit(0.4f, 0.4f);
			joint->setSwingLimitEnabled(true);
			joint->setTwistLimit(-0.1f, 0.1f);
			joint->setTwistLimitEnabled(true);
		}
		currentNode++;
	}
	return articulation;
}

// Scenes
void Physics::Controller() {
	myHitReport = new MyControllerHitReport();
	m_controller_manager = PxCreateControllerManager(*m_physics_scene);

	PxCapsuleControllerDesc desc;
	desc.height = 1.6f;
	desc.radius = 0.4f;
	desc.position.set(5, 0, 0);
	desc.material = m_physics_material;
	desc.reportCallback = myHitReport;
	desc.density = 100;

	m_playerController = m_controller_manager->createController(desc);
	m_playerController->setPosition(PxExtendedVec3(10, 0, 0));

	characterYVelocity = 0.0f;
	characterRotation = 0.0f;
	playerGravity = -0.5f;
	myHitReport->clearPlayerContactNormal();
	m_physics_scene->addActor(*m_playerController->getActor());
}
void Physics::movementController(float a_deltaTime) {
	float movementSpeed = 10.0f;
	float rotationSpeed = 1.0f;

	if (myHitReport->getPlayerContactNormal().y > 0.3f) {
		characterYVelocity = -0.1f;
	}
	else {
		characterYVelocity += playerGravity * a_deltaTime;
	}
	myHitReport->clearPlayerContactNormal();
	const PxVec3 up(0, 1, 0);

	PxVec3 velocity(0, characterYVelocity, 0);
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_UP) == GLFW_PRESS) {
		velocity.x -= movementSpeed * a_deltaTime;
	}
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_DOWN) == GLFW_PRESS) {
		velocity.x += movementSpeed * a_deltaTime;
	}
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_LEFT) == GLFW_PRESS){
		velocity.z += movementSpeed*a_deltaTime;
	}
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_RIGHT) == GLFW_PRESS){
		velocity.z -= movementSpeed*a_deltaTime;
	}

	float minDistance = 0.001f;
	PxControllerFilters filter;

	PxQuat rotation(characterRotation, PxVec3(0, 1, 0));

	m_playerController->move(rotation.rotate(velocity), minDistance, a_deltaTime, filter);
}
void MyControllerHitReport::onShapeHit(const PxControllerShapeHit &hit) {
	PxRigidActor* actor = hit.shape->getActor();
	playerContactNormal = hit.worldNormal;

	PxRigidDynamic* myActor = actor->is<PxRigidDynamic>();
	if (myActor) {

	}
}
void Physics::playGround() {
	PxTransform pose = PxTransform(PxVec3(0.0f, 0, 0.0f), PxQuat(PxHalfPi*1.0f, PxVec3(0.0f, 0.0f, 1.0f)));
	PxRigidStatic* plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physics_material);
	m_physics_scene->addActor(*plane);
	
	float density = 10;
	PxBoxGeometry box1(3, 3, 3);
	PxTransform transform1(PxVec3(20, 3, 0));
	PxRigidStatic* staticActor1 = PxCreateStatic(*m_physics, transform1, box1, *m_physics_material);
	setShapeAsTrigger(staticActor1);
	staticActor1->setName("Box1");
	m_physics_scene->addActor(*staticActor1);

	// Funnel
	PxBoxGeometry box2(2.7f, 0.1f, 1); // BaseFloorBottom
	PxTransform transform2(PxVec3(2.3f, 20, 0));
	PxRigidStatic* staticActor2 = PxCreateStatic(*m_physics, transform2, box2, *m_physics_material);
	m_physics_scene->addActor(*staticActor2);
	
	PxBoxGeometry box3(2, 1, 0.1f); // SideWallBottom
	PxTransform transform3(PxVec3(3, 21, 1));
	PxRigidStatic* staticActor3 = PxCreateStatic(*m_physics, transform3, box3, *m_physics_material);
	m_physics_scene->addActor(*staticActor3);
	
	PxBoxGeometry box4(2, 1, 0.1f); // SideWallBottom
	PxTransform transform4(PxVec3(3, 21, -1));
	PxRigidStatic* staticActor4 = PxCreateStatic(*m_physics, transform4, box4, *m_physics_material);
	m_physics_scene->addActor(*staticActor4);
	
	PxBoxGeometry box5(0.1f, 7, 1); // BackWallNeck
	PxTransform transform5(PxVec3(5, 27, 0));
	PxRigidStatic* staticActor5 = PxCreateStatic(*m_physics, transform5, box5, *m_physics_material);
	m_physics_scene->addActor(*staticActor5);
	
	PxBoxGeometry box6(0.1f, 6, 1); // FrontWallNeck
	PxTransform transform6(PxVec3(3, 28, 0));
	PxRigidStatic* staticActor6 = PxCreateStatic(*m_physics, transform6, box6, *m_physics_material);
	m_physics_scene->addActor(*staticActor6);
	
	PxBoxGeometry box7(2, 0.1f, 1); // BaseRoofBottom
	PxTransform transform7(PxVec3(1, 22, 0));
	PxRigidStatic* staticActor7 = PxCreateStatic(*m_physics, transform7, box7, *m_physics_material);
	m_physics_scene->addActor(*staticActor7);
	
	PxBoxGeometry box8(1, 6, 0.1f); // SideWallNeck
	PxTransform transform8(PxVec3(4, 28, -1));
	PxRigidStatic* staticActor8 = PxCreateStatic(*m_physics, transform8, box8, *m_physics_material);
	m_physics_scene->addActor(*staticActor8);
	
	PxBoxGeometry box9(1, 6, 0.1f); // SideWallNeck
	PxTransform transform9(PxVec3(4, 28, 1));
	PxRigidStatic* staticActor9 = PxCreateStatic(*m_physics, transform9, box9, *m_physics_material);
	m_physics_scene->addActor(*staticActor9);

	PxBoxGeometry box10(0.1f, 5, 1); // FrontBaseNeckWall
	PxTransform transform10(PxVec3(-1, 17, 0));
	PxRigidStatic* staticActor10 = PxCreateStatic(*m_physics, transform10, box10, *m_physics_material);
	m_physics_scene->addActor(*staticActor10);

	PxBoxGeometry box11(0.1f, 4, 1); // BackBaseNeckWall
	PxTransform transform11(PxVec3(1, 16, 0));
	PxRigidStatic* staticActor11 = PxCreateStatic(*m_physics, transform11, box11, *m_physics_material);
	m_physics_scene->addActor(*staticActor11);

	PxBoxGeometry box12(1, 5, 0.1f); // SideBaseNeckWall
	PxTransform transform12(PxVec3(0, 17, 1));
	PxRigidStatic* staticActor12 = PxCreateStatic(*m_physics, transform12, box12, *m_physics_material);
	m_physics_scene->addActor(*staticActor12);

	PxBoxGeometry box13(1, 5, 0.1f); // SideBaseNeckWall
	PxTransform transform13(PxVec3(0, 17, -1));
	PxRigidStatic* staticActor13 = PxCreateStatic(*m_physics, transform13, box13, *m_physics_material);
	m_physics_scene->addActor(*staticActor13);

	PxBoxGeometry box14(0.8f, 0.1f, 1); // PlatformBaseNeckWall
	PxTransform transform14(PxVec3(-0.3f, 19, 0));
	PxRigidStatic* staticActor14 = PxCreateStatic(*m_physics, transform14, box14, *m_physics_material);
	m_physics_scene->addActor(*staticActor14);

	PxBoxGeometry box15(0.8f, 0.1f, 1); // PlatformBaseNeckWall
	PxTransform transform15(PxVec3(0.3f, 18, 0));
	PxRigidStatic* staticActor15 = PxCreateStatic(*m_physics, transform15, box15, *m_physics_material);
	m_physics_scene->addActor(*staticActor15);

	PxBoxGeometry box16(0.8f, 0.1f, 1); // PlatformBaseNeckWall
	PxTransform transform16(PxVec3(-0.3f, 17, 0));
	PxRigidStatic* staticActor16 = PxCreateStatic(*m_physics, transform16, box16, *m_physics_material);
	m_physics_scene->addActor(*staticActor16);

	PxBoxGeometry box17(0.8f, 0.1f, 1); // PlatformBaseNeckWall
	PxTransform transform17(PxVec3(0.3f, 16, 0));
	PxRigidStatic* staticActor17 = PxCreateStatic(*m_physics, transform17, box17, *m_physics_material);
	m_physics_scene->addActor(*staticActor17);

	PxBoxGeometry box18(0.8f, 0.1f, 1); // PlatformBaseNeckWall
	PxTransform transform18(PxVec3(-0.3f, 15, 0));
	PxRigidStatic* staticActor18 = PxCreateStatic(*m_physics, transform18, box18, *m_physics_material);
	m_physics_scene->addActor(*staticActor18);

	PxBoxGeometry box19(0.8f, 0.1f, 1); // PlatformBaseNeckWall
	PxTransform transform19(PxVec3(0.3f, 14, 0));
	PxRigidStatic* staticActor19 = PxCreateStatic(*m_physics, transform19, box19, *m_physics_material);
	m_physics_scene->addActor(*staticActor19);

	PxBoxGeometry box20(0.8f, 0.1f, 1); // PlatformBaseNeckWall
	PxTransform transform20(PxVec3(-0.3f, 13, 0));
	PxRigidStatic* staticActor20 = PxCreateStatic(*m_physics, transform20, box20, *m_physics_material);
	m_physics_scene->addActor(*staticActor20);

	PxBoxGeometry box21(0.8f, 0.1f, 1); // PlatformBaseNeckWall
	PxTransform transform21(PxVec3(0.3f, 12, 0));
	PxRigidStatic* staticActor21 = PxCreateStatic(*m_physics, transform21, box21, *m_physics_material);
	m_physics_scene->addActor(*staticActor21);
}
void Physics::pool() {
	PxTransform pose = PxTransform(PxVec3(0.0f, 0, 0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
	PxRigidStatic* plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physics_material);
	const PxU32 numShapes = plane->getNbShapes();
	m_physics_scene->addActor(*plane);

	PxBoxGeometry side1(4.5f, 1.7f, 0.5f);
	PxBoxGeometry side2(0.5f, 1.7f, 4.5f);
	pose = PxTransform(PxVec3(0.0f, 0.5f, 4.0f));
	PxRigidStatic* box = PxCreateStatic(*m_physics, pose, side1, *m_physics_material);

	m_physics_scene->addActor(*box);
	m_PhysXActors.push_back(box);

	pose = PxTransform(PxVec3(0.0f, 0.5, -4.0f));
	box = PxCreateStatic(*m_physics, pose, side1, *m_physics_material);
	m_physics_scene->addActor(*box);
	m_PhysXActors.push_back(box);

	pose = PxTransform(PxVec3(4.0f, 0.5, 0));
	box = PxCreateStatic(*m_physics, pose, side2, *m_physics_material);
	m_physics_scene->addActor(*box);
	m_PhysXActors.push_back(box);

	pose = PxTransform(PxVec3(-4.0f, 0.5, 0));
	box = PxCreateStatic(*m_physics, pose, side2, *m_physics_material);
	m_physics_scene->addActor(*box);
	m_PhysXActors.push_back(box);

	PxParticleFluid* pf;

	PxU32 maxParticles = 4000;
	bool perParticleRestOffset = false;
	pf = m_physics->createParticleFluid(maxParticles, perParticleRestOffset);

	pf->setRestParticleDistance(.3f);
	pf->setDynamicFriction(0.1f);
	pf->setStaticFriction(0.1f);
	pf->setDamping(0.1f);
	pf->setParticleMass(0.1f);
	pf->setRestitution(0.0f);

	pf->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, true);
	pf->setStiffness(100);

	if (pf) {
		m_physics_scene->addActor(*pf);
		m_particleEmitter = new ParticleFluidEmitter(maxParticles, PxVec3(4, 28.5f, 0), pf, 0.01f);
		m_particleEmitter->setStartVelocityRange(-2.0f, 0, -2.0f, 2.0f, 2.0f, 2.0f);
	}
}
void Physics::ragdollScene() {
	PxArticulation* ragDollArticulation;
	ragDollArticulation = makeRagdoll(m_physics, m_ragdollData, PxTransform(PxVec3(0, 5, 10)), 0.2f, m_physics_material);
	m_physics_scene->addArticulation(*ragDollArticulation);
}
void Physics::triggerUpdate()
{
	if (mycollisionCallBack->SafeTrigger == 1)
	{
		mycollisionCallBack->SafeTrigger = 2;
		PxTransform box_transform = mycollisionCallBack->triggerActor->getGlobalPose();

		float muzzleSpeed = 5;
		glm::vec3 direction(0, 1, 0);
		physx::PxVec3 velocity = physx::PxVec3(direction.x, direction.y, direction.z) * muzzleSpeed;

		for (int k = 0; k < 10; ++k)
		{
			float muzzleSpeed = .05f;
			float density = 30;
			PxBoxGeometry Box(.5f, .5f, .5f);
			PxRigidDynamic* Bullet = PxCreateDynamic(*m_physics, box_transform, Box, *m_physics_material, density);

			glm::vec3 direction(1, 1, 0);
			physx::PxVec3 velocity = physx::PxVec3(direction.x, direction.y, direction.z) * muzzleSpeed;
			Bullet->setLinearVelocity(velocity, true);
			m_physics_scene->addActor(*Bullet);
		}
	}
}

// Triggers
void MyCollisionCallback::onContact(const PxContactPairHeader& pairHeader,
	const PxContactPair* pairs, PxU32 nbPairs) {
	for (PxU32 i = 0; i < nbPairs; i++) {
		const PxContactPair& cp = pairs[i];
		if (cp.events & PxPairFlag::eNOTIFY_TOUCH_FOUND) {
			std::cout << "Collision Detected between: ";
			std::cout << pairHeader.actors[0]->getName();
			std::cout << pairHeader.actors[1]->getName() << std::endl;
		}
	}
}
void MyCollisionCallback::onTrigger(PxTriggerPair* pairs, PxU32 nbPairs) {
	if (SafeTrigger == 0) {
		SafeTrigger = 1;
	}
	for (PxU32 i = 0; i < nbPairs; i++) {
		PxTriggerPair* pair = pairs + i;
		triggerActor = pair->triggerActor;
		PxActor* otherActor = pair->otherActor;
	}
}
void Physics::Trigger()
{
	PxActorTypeFlags desiredTypes = PxActorTypeFlag::eRIGID_DYNAMIC;
	PxU32 actor_count = m_physics_scene->getNbActors(desiredTypes);

	for (PxU32 i = 0; i < actor_count; ++i)
	{
		PxActor* actor;
		m_physics_scene->getActors(desiredTypes, &actor, 1, i);

		if (actor->isRigidActor())
		{
			playerActor = (PxRigidActor*)actor;

			setupFiltering(playerActor, FilterGroup::ePLAYER, FilterGroup::eGROUND);
			setupFiltering(playerActor, FilterGroup::ePLAYER, FilterGroup::eGROUND | FilterGroup::ePLATFORM);

			setupFiltering(playerActor, FilterGroup::eGROUND, FilterGroup::ePLAYER);

		}
	}
}