#ifndef SOUND_PROGRAMMING_H_
#define SOUND_PROGRAMMING_H_

#include "Application.h"
#include "Camera.h"
#include "Render.h"

#include <PxPhysicsAPI.h>
#include <PxScene.h>
#include <pvd/PxVisualDebugger.h>
#include "ParticleEmitter.h"
#include "ParticleFluidEmitter.h"

#include <atomic>

using namespace physx;

enum RagDollParts {
	NO_PARENT = -1,
	LOWER_SPINE,
	LEFT_PELVIS,
	RIGHT_PELVIS,
	LEFT_UPPER_LEG,
	RIGHT_UPPER_LEG,
	LEFT_LOWER_LEG,
	RIGHT_LOWER_LEG,
	UPPER_SPINE,
	LEFT_CLAVICLE,
	RIGHT_CLAVICLE,
	NECK,
	HEAD,
	LEFT_UPPER_ARM,
	RIGHT_UPPER_ARM,
	LEFT_LOWER_ARM,
	RIGHT_LOWER_ARM,
};

const PxVec3 X_AXIS = PxVec3(1, 0, 0);
const PxVec3 Y_AXIS = PxVec3(0, 1, 0);
const PxVec3 Z_AXIS = PxVec3(0, 0, 1);

struct RagdollNode {
	PxQuat globalRotation;

	PxVec3 scaledGlobalPos;

	int parentNodeIdx;
	float halfLength;
	float radius;
	float parentLinkPos;

	float childLinkPos;
	char* name;
	PxArticulationLink* linkPtr;

	RagdollNode(PxQuat _globalRotation, int _parentNodeIdx, float _halfLength,
		float _radius, float _parentLinkPos, float _childLinkPos, char* _name) {
		globalRotation = _globalRotation;
		parentNodeIdx = _parentNodeIdx;
		halfLength = _halfLength;
		radius = _radius;
		parentLinkPos = _parentLinkPos;
		childLinkPos = _childLinkPos;
		name = _name;
	}
};

struct FilterGroup {
	enum Enum {
		ePLAYER = (1 << 0),
		ePLATFORM = (1 << 1),
		eGROUND = (1 << 2)
	};
};

class MyCollisionCallback : public PxSimulationEventCallback {
public:
	std::atomic_int SafeTrigger;
	MyCollisionCallback() { SafeTrigger = 0; }
	PxRigidActor* triggerActor;
	virtual void onContact(const PxContactPairHeader& pairHeader,
		const PxContactPair* pairs, PxU32 nbPairs);
	virtual void onTrigger(PxTriggerPair* pairs, PxU32 nbPairs);
	virtual void onConstraintBreak(PxConstraintInfo*, PxU32) {};
	virtual void onWake(PxActor**, PxU32) {};
	virtual void onSleep(PxActor**, PxU32) {};
};

class MyControllerHitReport : public PxUserControllerHitReport {
public:
	virtual void onShapeHit(const PxControllerShapeHit &hit);
	virtual void onControllerHit(const PxControllersHit &hit) {};
	virtual void onObstacleHit(const PxControllerObstacleHit &hit) {};

	MyControllerHitReport() :PxUserControllerHitReport() {};
	PxVec3 getPlayerContactNormal() { return playerContactNormal; };
	void clearPlayerContactNormal() { playerContactNormal = PxVec3(0, 0, 0); };
	PxVec3 playerContactNormal;
};

class Physics : public Application
{
public:
	virtual bool startup();
	virtual void shutdown();
    virtual bool update();
    virtual void draw();
	void AddWidget(PxShape* shape, PxRigidActor* actor, vec4 geo_color);
	void renderGizmos(PxScene* physics_scene);
	void setupPhysX();

	void Controller();
	void movementController(float a_deltaTime);
	void playGround();
	void pool();
	void ragdollScene();
	void Trigger();
	void triggerUpdate();

	PxScene* createDefaultScene();
    Renderer* m_renderer;
    FlyCamera m_camera;
    float m_delta_time;

	float characterYVelocity;
	float characterRotation;
	float playerGravity;

	int counter = 10;

	PxRigidActor* playerActor;
	RagdollNode** m_ragdollData;

	PxFoundation* m_physics_foundation;
	PxPhysics* m_physics;
	PxScene* m_physics_scene;

	PxDefaultErrorCallback m_default_error_callback;
	PxDefaultAllocator m_default_allocator;
	PxSimulationFilterShader m_default_filter_shader;

	PxMaterial* m_physics_material;
	PxCooking* m_physics_cooker;
	PxControllerManager* m_controller_manager;
	PxController* m_playerController;

	ParticleFluidEmitter* m_particleEmitter;

	PxBoxGeometry m_boxGeometry;
	PxSphereGeometry m_geometry;
	PxCapsuleGeometry m_capsuleGeometry;

	std::vector<PxRigidStatic*>m_PhysXActors;

	MyControllerHitReport* myHitReport;

	PxArticulation* makeRagdoll(PxPhysics* g_Physics, RagdollNode** nodeArray,
		PxTransform worldPos, float scaleFactor, PxMaterial* ragdollMaterial);

	MyCollisionCallback* mycollisionCallBack;
};

class myAllocator : public PxAllocatorCallback {
public:
	virtual ~myAllocator() {}
	virtual void* allocate(size_t size, const char* typeName, const char* filename, int line) {
		void* pointer = _aligned_malloc(size, 16);
		return pointer;
	}
	virtual void deallocate(void* ptr) {
		_aligned_free(ptr);
	}
};

static PxFilterFlags myFilterShader(PxFilterObjectAttributes attributes0, PxFilterData filterData0,
	PxFilterObjectAttributes attributes1, PxFilterData filterData1,
	PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBloackSize) {
	if (PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1)) {
		pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
		return PxFilterFlag::eDEFAULT;
	}
	pairFlags = PxPairFlag::eCONTACT_DEFAULT;
	if ((filterData0.word0 & filterData1.word1) && (filterData1.word0 & filterData0.word1)) {
		pairFlags |= PxPairFlag::eNOTIFY_TOUCH_FOUND | PxPairFlag::eNOTIFY_TOUCH_LOST;
	}
	return PxFilterFlag::eDEFAULT;
}
#endif //CAM_PROJ_H_