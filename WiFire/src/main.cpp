#include "TestApplication.h"
#include <glfw3.h>

int main() {
	
	WiFire* app = new TestApplication();
	if (app->startup())
		app->run();
	app->shutdown();

	return 0;
}