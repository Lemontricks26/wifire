#pragma once
#include "WiFire.h"
#include <iostream>
#include <vector>

enum ShapeType
{
	PLANE = 0,
	SPHERE = 1,
	BOX = 2,
	NUMBERSHAPE = 3,
	JOINT = 4,
};

class PhysicsObject {
public:
	ShapeType shapeID;
	vec4 m_colour;
	float m_mass;
	bool isKinematic;
	void virtual update(vec3 gravity, float timeStep) = 0;
	void virtual debug() = 0;
	void virtual makeGizmo() = 0;
	void virtual resetPosition() {};
};

class RigidBody : public PhysicsObject {
public:
	RigidBody() {}
	RigidBody(vec3 position, vec3 velocity,
			float rotation, float mass, vec4 colour, bool kinematic);
	~RigidBody() {}

	vec3 m_position;
	vec3 m_velocity;
	vec3 m_force;
	vec3 m_acceleration;

	virtual void update(vec3 gravity, float timeStep);
	virtual void debug();
	void applyForce(vec3 force);
	void applyForceToActor(RigidBody* actor2, vec3 force);
};

class SphereClass : public RigidBody {
public:
	float m_radius;
	SphereClass() {}
	SphereClass(vec3 position,
				vec3 velocity,
				float mass, float radius,
				vec4 colour, bool kinematic);
	~SphereClass() {}
	virtual void makeGizmo();
};

class BoxClass : public RigidBody {
public:
	float m_length;
	float m_height;
	float m_width;
	BoxClass() {}
	BoxClass(vec3 position,
			 vec3 velocity,
			 float mass, float length, float height, float width,
			 vec4 colour, bool kinematic);
	~BoxClass() {}
	virtual void makeGizmo();
};

class PlaneClass : public PhysicsObject {
public:
	vec3 m_normal;
	float m_distance;
	void virtual update(vec3 gravity, float timeStep) {};
	void virtual debug() {};
	void virtual makeGizmo();
	PlaneClass(vec3 normal, float distance);
	PlaneClass() {}
	~PlaneClass() {}
};

class SpringJoint : public PhysicsObject {
public:
	SpringJoint() {}
	SpringJoint(RigidBody* connection1, RigidBody* connection2,
		float springCoefficient, float damping);
	~SpringJoint() {}
private:

	void virtual update(vec3 gravity, float timeStep);
	void virtual debug();
	void virtual makeGizmo();

	RigidBody* connections[2];
	float m_damping;
	float m_restLength;
	float m_springCoefficient;
};

class PhysicScene {
public:
	vec3 gravity;
	float timeStep;
	std::vector<PhysicsObject*> actors;
	void addActor(PhysicsObject* actor);
	void removeActor(PhysicsObject* actor);
	void update();
	void debugScene();
	void checkForCollision();
	static void CollisionSeperate(PhysicsObject* obj1, PhysicsObject* obj2, float overlap, vec3 normal);
	static void CollisionResponse(PhysicsObject* obj1, PhysicsObject* obj2, float overlap, vec3 normal);
	static void DynamicDynamicCollision(PhysicsObject* obj1, PhysicsObject* obj2, float overlap, vec3 normal);
	static void DynamicStaticCollision(PhysicsObject* obj1, PhysicsObject* obj2, float overlap, vec3 normal);
	static bool sphere2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool sphere2Plane(PhysicsObject* sphere, PhysicsObject* plane);
	static bool sphere2Box(PhysicsObject* sphere, PhysicsObject* box);
	static bool plane2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool plane2Sphere(PhysicsObject* plane, PhysicsObject* sphere);
	static bool plane2Box(PhysicsObject* plane, PhysicsObject* box);
	static bool box2Plane(PhysicsObject* box, PhysicsObject* plane);
	static bool box2Sphere(PhysicsObject* box, PhysicsObject* sphere);
	static bool box2Box(PhysicsObject* obj1, PhysicsObject* obj2);
};