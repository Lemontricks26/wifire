#pragma once

#include "WiFire.h"

#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"
#include "Phengine.h"

class Camera;
class FBXLoader;
class Grid;
class DIYPhysicScene;
class SphereClass;
class PlaneClass;
class BoxClass;

class TestApplication : public WiFire {
public:
	TestApplication();
	virtual ~TestApplication();

	virtual bool startup();
	virtual void shutdown();

	virtual bool update(float deltaTime);
	virtual void draw();

	void getFrustumPlanes(const mat4& transform, vec4* planes);

private:

	Camera*					m_camera;
	FBXLoader*				m_fbxLoader;
	Grid*					m_grid;

	vec3					m_clearColour;
	mat4					m_projectionViewMatrix;
	mat4					m_transformPosWar;

	PhysicScene*			physicsScene;
	SphereClass*			ball0;
	SphereClass*			ball1;
	SphereClass*			ball2;
	SphereClass*			ball3;
	SphereClass*			ball4;
	SphereClass*			ball5;
	SphereClass*			ball6;
	SphereClass*			ball7;
	SphereClass*			ball8;
	SphereClass*			ball9;
	SphereClass*			ball10;
	SphereClass*			ball11;
	PlaneClass*				plane;
	BoxClass*				box0;
	BoxClass*				box1;

	SpringJoint*			line1;
	SpringJoint*			line2;
	SpringJoint*			line3;
	SpringJoint*			line4;
	SpringJoint*			line5;

	float					position;

	unsigned int			m_program;
	unsigned int			indexCount;
};