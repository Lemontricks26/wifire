#ifndef FBXLOADER_H
#define FBXLOADER_H

#include "FBXFile.h"
#include <iostream>

class Camera;

class FBXLoader
{
public:
	FBXLoader() {
		m_fbx = new FBXFile();
	}
	~FBXLoader() {
	}

	struct Vertex {
		float x, y, z, w;
		float nx, ny, nz, nw;
		float tx, ty, tz, tw;
		float s, t;
	};

	void		createOpenGLBuffers();
	void		cleanupOpenGLBuffers();
	void		drawData();
	void		updateTransform(glm::mat4 transform);
	void		render(Camera* camera);

	void		startUpPNG(const char* file, const char* texture);
	void		startUpTGA(const char* file, const char* texture);
	void		draw(Camera* camera);
	void		shutdown();


private:
	void		shader();

	unsigned int			m_program;
	unsigned int			m_VAO;
	unsigned int			m_VBO;
	unsigned int			m_IBO;
	unsigned int			m_indexCount;
	unsigned int			m_diffuse;
	FBXFile* m_fbx;
};
#endif