#ifndef WIFIRE_H
#define WIFIRE_H

struct GLFWwindow;

#include "gl_core_4_4.h"
#include <glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/vec3.hpp>
#include <stb_image.h>
#include <tiny_obj_loader.h>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat2;
using glm::mat3;
using glm::mat4;

class WiFire {
public:
	WiFire() {}
	virtual ~WiFire() {}

	void run();
	
	virtual bool startup() = 0;
	virtual void shutdown() = 0;
				 
	virtual bool update(float deltaTime) = 0;
	virtual void draw() = 0;

protected:

	virtual bool		createWindow(const char* title, int width, int height);
	virtual void		destroyWindow();

	unsigned int		m_VAO, m_VBO, m_IBO;

	GLFWwindow*			m_window;
};
#endif