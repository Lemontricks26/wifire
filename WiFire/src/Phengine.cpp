#include "Phengine.h"
#include "Gizmos.h"
#include <cmath>
#include <algorithm>

RigidBody::RigidBody(vec3 position, vec3 velocity, float rotation, float mass, vec4 colour, bool kinematic) {
	m_position = position;
	m_velocity = velocity;
	isKinematic = kinematic;
	m_mass = mass;
	m_colour = colour;
}
void RigidBody::update(vec3 gravity, float timeStep) {
	if (!isKinematic)
	{
		m_acceleration += gravity;
		m_velocity += m_acceleration * timeStep;
		m_position += m_velocity * timeStep;
	}
	m_acceleration = vec3(0);
}
void RigidBody::debug() {
	std::cout << "Position " << m_position.x << ',' << m_position.y << ',' << m_position.z << std::endl;
}
void RigidBody::applyForce(glm::vec3 force) {
	m_acceleration += (force / m_mass);
}
void RigidBody::applyForceToActor(RigidBody* actor2, vec3 force) {
	actor2->applyForce(force);
	applyForce(-force);
}

SphereClass::SphereClass(vec3 position, glm::vec3 velocity, float mass, float radius, vec4 colour, bool kinematic) {
	m_position = position;
	m_velocity = velocity;
	m_mass = mass;
	m_radius = radius;
	isKinematic = kinematic;
	m_colour = colour;
	shapeID = SPHERE;
}
void SphereClass::makeGizmo() {
	Gizmos::addSphere(m_position, m_radius, 16, 16, m_colour);
}

BoxClass::BoxClass(glm::vec3 position, vec3 velocity, float mass, float length, float height, float width, vec4 colour, bool kinematic) {
	m_position = position;
	m_velocity = velocity;
	m_mass = mass;
	m_length = length;
	m_height = height;
	m_width = width;
	isKinematic = kinematic;
	m_colour = colour;
	shapeID = BOX;
}
void BoxClass::makeGizmo() {
	vec3 size(m_length, m_height, m_width);
	Gizmos::addAABB(m_position, size, m_colour);
}

PlaneClass::PlaneClass(vec3 normal, float distance) {
	m_normal = normal;
	m_distance = distance;
	m_mass = INFINITY;
	shapeID = PLANE;
	isKinematic = true;
}
void PlaneClass::makeGizmo() {
	vec3 perpNorm = vec3(m_normal.z, m_normal.y, -m_normal.x);
	vec3 perpNorm2 = cross(m_normal, perpNorm);

	vec3 centrePoint = m_normal * m_distance;

	mat4 t(1);
	t[0] = vec4(perpNorm, 0);
	t[1] = vec4(m_normal, 0);
	t[2] = vec4(perpNorm2, 0);

	vec4 colour(1, 1, 1, 1);
	
	Gizmos::addDisk(centrePoint, 10, 4, colour, &t);
	Gizmos::addTransform(translate(centrePoint) * t);
}

void SpringJoint::debug() {}
SpringJoint::SpringJoint(RigidBody* connection1, RigidBody* connection2, float springCoefficient, float damping) {
	connections[0] = connection1;
	connections[1] = connection2;
	m_springCoefficient = springCoefficient;
	m_damping = damping;
	m_restLength = length(connections[0]->m_position - connections[1]->m_position);
	shapeID = JOINT;
}
void SpringJoint::update(vec3 gravity, float timeStep){
	vec3 displacement = connections[0]->m_position - connections[1]->m_position;
	vec3 positionDirection = normalize(displacement);
	vec3 restingDisplacement = positionDirection * m_restLength;
	vec3 dampingVector = m_damping * (connections[1]->m_velocity - connections[0]->m_velocity);
	connections[1]->applyForce(-m_springCoefficient * (restingDisplacement - displacement) - dampingVector);

	//float displacement = glm::distance(connections[0]->m_position, connections[1]->m_position) - m_restLength;
	//vec3 v = connections[0]->m_velocity - connections[1]->m_velocity * m_damping;
	//
	//vec3 Force = (-m_springCoefficient * displacement) - v;
	//
	//connections[1]->applyForceToActor(connections[0], glm::normalize(connections[0]->m_position - connections[1]->m_position) * Force);
}
void SpringJoint::makeGizmo() {
	Gizmos::addLine(connections[0]->m_position, connections[1]->m_position, vec4(1, 1, 1, 1));
}

void PhysicScene::addActor(PhysicsObject* actor) {
	actors.push_back(actor);
}
void PhysicScene::removeActor(PhysicsObject* actor) {
	auto item = std::find(actors.begin(), actors.end(), actor);
	if (item < actors.end()) {
		actors.erase(item);
	}
}
void PhysicScene::update() {
	for (auto actorPtr : actors) {
		actorPtr->update(gravity, timeStep);
		actorPtr->makeGizmo();
		checkForCollision();
	}
}
void PhysicScene::debugScene() {
	int count = 0;
	for (auto actorPtr : actors) {
		std::cout << count << " : ";
		actorPtr->debug();
		count++;
	}
}

bool PhysicScene::sphere2Sphere(PhysicsObject* obj1, PhysicsObject* obj2) {
	SphereClass* sphere1 = dynamic_cast<SphereClass*>(obj1);
	SphereClass* sphere2 = dynamic_cast<SphereClass*>(obj2);

	if (sphere1 != NULL && sphere2 != NULL)
	{
		vec3 delta = sphere2->m_position - sphere1->m_position;
		float distance = length(delta);
		float intersection = sphere1->m_radius + sphere2->m_radius - distance;
		if (intersection > 0)
		{
			vec3 collisionNormal = normalize(delta);
			vec3 relativeVelocity = sphere1->m_velocity - sphere2->m_velocity;
			vec3 collisionVector = collisionNormal * (dot(relativeVelocity, collisionNormal));
			vec3 forceVector = collisionVector * 1.0f / (1 / sphere1->m_mass + 1 / sphere2->m_mass);
			CollisionResponse(sphere1, sphere2, intersection, collisionNormal);
			return true;
		}
	}
	return false;
}
bool PhysicScene::sphere2Plane(PhysicsObject* sphere, PhysicsObject* plane) {
	return plane2Sphere(plane, sphere);
}
bool PhysicScene::sphere2Box(PhysicsObject* sphere, PhysicsObject* box) {
	SphereClass* sphere1 = dynamic_cast<SphereClass*>(sphere);
	BoxClass* box1 = dynamic_cast<BoxClass*>(box);

	if (sphere1 != NULL && box1 != NULL)
	{
		vec3 box1MinExtents = -vec3(box1->m_length/2, box1->m_height/2, box1->m_width/2);
		vec3 box1MaxExtents = vec3(box1->m_length / 2, box1->m_height / 2, box1->m_width / 2);

		vec3 distance = sphere1->m_position - box1->m_position;
		vec3 clampPoint = distance;

		if (distance.x < box1MinExtents.x)
			clampPoint.x = box1MinExtents.x;
		else if (distance.x > box1MaxExtents.x)
			clampPoint.x = box1MaxExtents.x;

		if (distance.y < box1MinExtents.y)
			clampPoint.y = box1MinExtents.y;
		else if (distance.y > box1MaxExtents.y)
			clampPoint.y = box1MaxExtents.y;

		if (distance.z < box1MinExtents.z)
			clampPoint.z = box1MinExtents.z;
		else if (distance.z > box1MaxExtents.z)
			clampPoint.z = box1MaxExtents.z;

		vec3 clampedDistance = distance - clampPoint;

		float overlap = glm::length(clampedDistance) - sphere1->m_radius;

		if (overlap < 0)
		{
			vec3 collisionNormal = normalize(distance);
			vec3 relativeVelocity = sphere1->m_velocity - box1->m_velocity;
			vec3 collisionVector = collisionNormal * (dot(relativeVelocity, collisionNormal));
			vec3 forceVector = collisionVector * 1.0f / (1 / sphere1->m_mass + 1 / box1->m_mass);

			CollisionResponse(box1, sphere1, -overlap, glm::normalize(clampedDistance));

			return true;
		}
		return false;
	}
	return false;
}

bool PhysicScene::plane2Plane(PhysicsObject* obj1, PhysicsObject* obj2) {
	return false;
}
bool PhysicScene::plane2Sphere(PhysicsObject* plane, PhysicsObject* sphere) {
	SphereClass* sphere1 = dynamic_cast<SphereClass*>(sphere);
	PlaneClass* plane1 = dynamic_cast<PlaneClass*>(plane);

	if (plane1 != NULL && sphere1 != NULL)
	{
		vec3 collisionNormal = plane1->m_normal;
		float sphereToPlane = dot(sphere1->m_position, plane1->m_normal) - plane1->m_distance;
	
		if (sphereToPlane < 0)
		{
			collisionNormal *= -1;
			sphereToPlane *= -1;
		}

		float intersection = sphere1->m_radius - sphereToPlane;

		if (intersection > 0)
		{
			vec3 planeNormal = plane1->m_normal;
			if (sphereToPlane < 0)
			{
				planeNormal *= -1;
			}
	
			CollisionResponse(plane, sphere, intersection, planeNormal);
			return true;
		}
	}
	return false;
}
bool PhysicScene::plane2Box(PhysicsObject* plane, PhysicsObject* box) {
	BoxClass* box1 = dynamic_cast<BoxClass*>(box);
	PlaneClass* plane1 = dynamic_cast<PlaneClass*>(plane);

	if (plane1 != NULL && box1 != NULL && !box1->isKinematic)
	{
		float Box1Minx = box1->m_position.x - (box1->m_length / 2);
		float Box1Maxx = box1->m_position.x + (box1->m_length / 2);
		float Box1Miny = box1->m_position.y - (box1->m_height / 2);
		float Box1Maxy = box1->m_position.y + (box1->m_height / 2);
		float Box1Minz = box1->m_position.z - (box1->m_width / 2);
		float Box1Maxz = box1->m_position.z + (box1->m_width / 2);

		vec3 boxMin = vec3(Box1Minx, Box1Miny, Box1Minz);
		vec3 boxMax = vec3(Box1Maxx, Box1Maxy, Box1Maxz);

		float minPointDistanceAlongPlaneNormal = dot(boxMin, plane1->m_normal);
		float maxPointDistanceAlongPlaneNormal = dot(boxMax, plane1->m_normal);

		float overlap = std::min(minPointDistanceAlongPlaneNormal, maxPointDistanceAlongPlaneNormal);

		if (overlap < 0)
		{
			float boxToPlane = dot(box1->m_position, plane1->m_normal) - plane1->m_distance;
			vec3 planeNormal = plane1->m_normal;
			if (boxToPlane < 0)
			{
				planeNormal *= -1;
			}
			CollisionResponse(plane, box, -overlap, plane1->m_normal);

			return true;
		}
	}
	return false;
}

bool PhysicScene::box2Sphere(PhysicsObject* box, PhysicsObject* sphere) {
	return PhysicScene::sphere2Box(sphere, box);
}
bool PhysicScene::box2Plane(PhysicsObject* box, PhysicsObject* plane) {
	return plane2Box(plane, box);
}
bool PhysicScene::box2Box(PhysicsObject* obj1, PhysicsObject* obj2) {
	BoxClass* box1 = dynamic_cast<BoxClass*>(obj1);
	BoxClass* box2 = dynamic_cast<BoxClass*>(obj2);

	if (box1 != NULL && box2 != NULL)
	{
		float Box1Minx = box1->m_position.x - (box1->m_length / 2);
		float Box1Maxx = box1->m_position.x + (box1->m_length / 2);
		float Box1Miny = box1->m_position.y - (box1->m_height / 2);
		float Box1Maxy = box1->m_position.y + (box1->m_height / 2);
		float Box1Minz = box1->m_position.z - (box1->m_width / 2);
		float Box1Maxz = box1->m_position.z + (box1->m_width / 2);

		float Box2Minx = box2->m_position.x - (box2->m_length / 2);
		float Box2Maxx = box2->m_position.x + (box2->m_length / 2);
		float Box2Miny = box2->m_position.y - (box2->m_height / 2);
		float Box2Maxy = box2->m_position.y + (box2->m_height / 2);
		float Box2Minz = box2->m_position.z - (box2->m_width / 2);
		float Box2Maxz = box2->m_position.z + (box2->m_width / 2);

		vec3 box1Min = vec3(Box1Minx, Box1Miny, Box1Minz);
		vec3 box1Max = vec3(Box1Maxx, Box1Maxy, Box1Maxz);

		vec3 box2Min = vec3(Box2Minx, Box2Miny, Box2Minz);
		vec3 box2Max = vec3(Box2Maxx, Box2Maxy, Box2Maxz);

		if (box1Min.x < box2Max.x && box1Max.x > box2Min.x && box1Min.y < box2Max.y && box1Max.y > box2Min.y && box1Min.z < box2Max.z && box1Max.z > box2Min.z)
		{
			vec3 boxDelta = box2->m_position - box1->m_position;
			vec3 boxExtents = vec3(box1->m_length / 2 + box2->m_length / 2, box1->m_height / 2 + box2->m_height / 2, box1->m_width / 2 + box2->m_width / 2);


			float xOverlap = std::abs(boxDelta.x) - boxExtents.x;
			float yOverlap = std::abs(boxDelta.y) - boxExtents.y;
			float zOverlap = std::abs(boxDelta.z) - boxExtents.z;

			float minOverlap = xOverlap;
			minOverlap = yOverlap < 0 ? std::max(minOverlap, yOverlap) : minOverlap;
			minOverlap = zOverlap < 0 ? std::max(minOverlap, zOverlap) : minOverlap;

			vec3 seperationNormal(0);

			if (xOverlap == minOverlap) seperationNormal.x = std::signbit(boxDelta.x) ? -1.0f : 1.0f;
			else if (yOverlap == minOverlap) seperationNormal.y = std::signbit(boxDelta.y) ? -1.0f : 1.0f;
			else if (zOverlap == minOverlap) seperationNormal.z = std::signbit(boxDelta.z) ? -1.0f : 1.0f;

			CollisionResponse(obj1, obj2, -minOverlap, seperationNormal);
			return true;
		}
	}
	return false;
}

typedef bool(*fn)(PhysicsObject*, PhysicsObject*);
static fn collisionFunctionArray[] = {
	PhysicScene::plane2Plane,	PhysicScene::plane2Sphere,	PhysicScene::plane2Box,
	PhysicScene::sphere2Plane,	PhysicScene::sphere2Sphere,	PhysicScene::sphere2Box,
	PhysicScene::box2Plane,		PhysicScene::box2Sphere,	PhysicScene::box2Box
};
void PhysicScene::checkForCollision() {
	int actorCount = actors.size();
	for (int outer = 0; outer < actorCount - 1; outer++) {
		for (int inner = outer + 1; inner < actorCount; inner++) {
			PhysicsObject* object1 = actors[outer];
			PhysicsObject* object2 = actors[inner];
			int _shapeID1 = object1->shapeID;
			int _shapeID2 = object2->shapeID;
			if (_shapeID1 != JOINT && _shapeID2 != JOINT) {
				int functionIndex = (_shapeID1 * NUMBERSHAPE) + _shapeID2;
				fn collisionFunctionPtr = collisionFunctionArray[functionIndex];
				if (collisionFunctionPtr != NULL) {
					collisionFunctionPtr(object1, object2);
				}
			}
		}
	}
}

void PhysicScene::CollisionSeperate(PhysicsObject* obj1, PhysicsObject* obj2, float overlap, vec3 normal)
{
	float totalMass = obj1->m_mass + obj2->m_mass;
	float massRatio1 = obj1->m_mass / totalMass;
	float massRatio2 = obj2->m_mass / totalMass;

	if (obj1->isKinematic)
	{
		massRatio1 = 0;
		massRatio2 = 1;
	}
	else if (obj2->isKinematic)
	{
		massRatio1 = 1;
		massRatio2 = 0;
	}

	vec3 seperationVector = normal * overlap;

	RigidBody* body1 = dynamic_cast<RigidBody*>(obj1);
	if (body1 != nullptr)
		body1->m_position += (-seperationVector * massRatio1);

	RigidBody* body2 = dynamic_cast<RigidBody*>(obj2);
	if (body2 != nullptr)
		body2->m_position += (seperationVector * massRatio2);
}
void PhysicScene::CollisionResponse(PhysicsObject* obj1, PhysicsObject* obj2, float overlap, vec3 normal)
{
	if ((obj1->isKinematic == true) && (obj2->isKinematic == true))
		return;

	CollisionSeperate(obj1, obj2, overlap, normal);

	if (obj2->isKinematic)
		DynamicStaticCollision(obj1, obj2, overlap, normal);
	else if (obj1->isKinematic)
		DynamicStaticCollision(obj2, obj1, overlap, -normal);
	else
		DynamicDynamicCollision(obj1, obj2, overlap, normal);
}
void PhysicScene::DynamicDynamicCollision(PhysicsObject* obj1, PhysicsObject* obj2, float overlap, vec3 normal)
{
	RigidBody* object1 = dynamic_cast<RigidBody*>(obj1);
	RigidBody* object2 = dynamic_cast<RigidBody*>(obj2);

	vec3 relativeVelocity = object2->m_velocity - object1->m_velocity;

	float velocityAlongNormal = glm::dot(relativeVelocity, normal);
	float impuseAmount = -(2) * velocityAlongNormal;

	impuseAmount /= 1 / object1->m_mass + 1 / object2->m_mass;

	vec3 impulse = impuseAmount * normal * 0.9f;
	object1->m_velocity += (1 / object1->m_mass * -impulse);
	object2->m_velocity += (1 / object2->m_mass * +impulse);
}
void PhysicScene::DynamicStaticCollision(PhysicsObject* obj1, PhysicsObject* obj2, float overlap, vec3 normal)
{
	RigidBody* object1 = dynamic_cast<RigidBody*>(obj1);
	vec3 relativeVelocity = object1->m_velocity;

	float velocityAlongNormal = glm::dot(relativeVelocity, normal);
	float impulseAmount = -(-2) * velocityAlongNormal;

	impulseAmount /= (1 / obj1->m_mass + 1 / obj2->m_mass);

	vec3 impulse = impulseAmount * normal * 0.9f;
	object1->m_velocity += (1 / object1->m_mass * -impulse);
}