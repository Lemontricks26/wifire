#include "TestApplication.h"
#include "Gizmos.h"
#include "Camera.h"
#include "FBXLoader.h"
#include "Grid.h"

TestApplication::TestApplication()
	: m_camera(nullptr) {
	m_fbxLoader = new FBXLoader();
	m_grid = new Grid();

	ball0 = new SphereClass();
	ball1 = new SphereClass();
	ball2 = new SphereClass();
	ball3 = new SphereClass();
	ball4 = new SphereClass();
	ball5 = new SphereClass();
	ball6 = new SphereClass();
	ball7 = new SphereClass();
	ball8 = new SphereClass();
	ball9 = new SphereClass();
	ball1 = new SphereClass();
	plane = new PlaneClass();
	box0 = new BoxClass();
	box1 = new BoxClass();

	line1 = new SpringJoint();
	line2 = new SpringJoint();
	line3 = new SpringJoint();
	line4 = new SpringJoint();
	line5 = new SpringJoint();

	physicsScene = new PhysicScene();
}

TestApplication::~TestApplication() {
	delete m_fbxLoader;
	delete m_grid;

	delete ball0;
	delete ball1;
	delete ball2;
	delete ball3;
	delete ball4;
	delete ball5;
	delete ball6;
	delete ball7;
	delete ball8;
	delete ball9;
	delete ball1;
	delete plane;
	delete box0;
	delete box1;
	delete line1;
	delete line2;
	delete line3;
	delete line4;
	delete line5;

	delete physicsScene;
}

bool TestApplication::startup() {

	// create a basic window
	createWindow("WiFire Test Engine", 1280, 720);

	// start the gizmo system that can draw basic shapes
	Gizmos::create();

	// create a camera
	m_camera = new Camera(glm::pi<float>() * 0.25f, 16 / 9.f, 0.1f, 1000.f);
	m_camera->setLookAtFrom(vec3(10, 10, 10), vec3(0));

#pragma region StartupCode

	m_grid->generateGrid(200, 200);

	//m_fbxLoader->startUpPNG("./Model/Wargreymon/WarGreymon.fbx", "./Model/Wargreymon/WarGreymon.fbm/diss_00.png");
	//m_transformPosWar *= scale(vec3(0.01f));
	//positionTwo = -600;

	physicsScene->gravity = vec3(0, -10, 0);
	physicsScene->timeStep = 0.1f;

	box0 = new BoxClass(vec3(0, 20, 5), vec3(0, 0, 0), 1, 2, 2, 2, vec4(1, 1, 1, 1), false);

	ball0 = new SphereClass(vec3(0, 50, -10), vec3(0, -1, 0), 20, 2, vec4(1, 0, 0, 1), true); //Red
	ball1 = new SphereClass(vec3(4, 50, -10), vec3(0, -1, 0), 20, 2, vec4(0, 1, 0, 1), true); //Green
	ball2 = new SphereClass(vec3(-4, 50, -10), vec3(0, -1, 0), 20, 2, vec4(0, 0, 1, 1), true); //Blue
	ball3 = new SphereClass(vec3(8, 50, -10), vec3(0, -1, 0), 20, 2, vec4(1, 0, 1, 1), true); //Pink
	ball4 = new SphereClass(vec3(-8, 50, -10), vec3(0, -1, 0), 20, 2, vec4(1, 1, 0, 1), true); //Yellow
	
	ball5 = new SphereClass(vec3(0, 20, -10), vec3(0, -1, 0), 2, 2, vec4(1, 0, 0, 1), false); //Red
	ball6 = new SphereClass(vec3(4, 20, -10), vec3(0, -1, 0), 2, 2, vec4(0, 1, 0, 1), false); //Green
	ball7 = new SphereClass(vec3(-4, 20, -10), vec3(0, -1, 0), 2, 2, vec4(0, 0, 1, 1), false); //Blue
	ball8 = new SphereClass(vec3(8, 20, -10), vec3(0, -1, 0), 2, 2, vec4(1, 0, 1, 1), false); //Pink
	ball9 = new SphereClass(vec3(-38, 50, -10), vec3(0, -1, 0), 2, 2, vec4(1, 1, 0, 1), false); //Yellow

	ball10 = new SphereClass(vec3(0, 50, 0), vec3(0, -1, 0), 20, 2, vec4(1, 1, 0, 1), false);

	plane = new PlaneClass(vec3(0, 1, 0), 0);

	line1 = new SpringJoint(ball0, ball5, 100, 1);
	line2 = new SpringJoint(ball1, ball6, 100, 1);
	line3 = new SpringJoint(ball2, ball7, 100, 1);
	line4 = new SpringJoint(ball3, ball8, 100, 1);
	line5 = new SpringJoint(ball4, ball9, 100, 1);

	physicsScene->addActor(box0);

	physicsScene->addActor(ball0);
	physicsScene->addActor(ball1);
	physicsScene->addActor(ball2);
	physicsScene->addActor(ball3);
	physicsScene->addActor(ball4);
	physicsScene->addActor(ball5);
	physicsScene->addActor(ball6);
	physicsScene->addActor(ball7);
	physicsScene->addActor(ball8);
	physicsScene->addActor(ball9);
	physicsScene->addActor(ball10);

	physicsScene->addActor(plane);

	physicsScene->addActor(line1);
	physicsScene->addActor(line2);
	physicsScene->addActor(line3);
	physicsScene->addActor(line4);
	physicsScene->addActor(line5);

#pragma endregion StartupCode

	return true;
}

void TestApplication::shutdown() {

	//////////////////////////////////////////////////////////////////////////
	// SHUTDOWN CODE
	
	//m_fbxLoader->shutdown();

	//////////////////////////////////////////////////////////////////////////

	// delete our camera and cleanup gizmos
	delete m_camera;
	Gizmos::destroy();

	// destroy our window properly
	destroyWindow();
}

bool TestApplication::update(float deltaTime) {

	// close the application if the window closes
	if (glfwWindowShouldClose(m_window) ||
		glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		return false;

	Gizmos::clear();
	// update the camera's movement
	m_camera->update(deltaTime);

	//m_fbxLoader->updateTransform(translate(m_transformPosWar, vec3(positionTwo, 200, 100)));

	if (glfwGetKey(m_window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		mat4 temp = m_camera->getProjectionView();
		vec3 dir = vec3(temp[2]);
		vec3 vel = vec3(dir.x, dir.y, dir.z) * 50;
		mat4 temp2 = m_camera->getTransform();
		vec3 pos = vec3(m_camera->getTransform()[3]);
		SphereClass* shot = new SphereClass(pos, vel, 1, 4, vec4(1, 0, 0, 1), false);
		physicsScene->addActor(shot);
	}

	physicsScene->update();
	std::cout << ball5->m_position.y << std::endl;

	// clear the gizmos out for this frame
	Gizmos::draw(m_camera->getProjectionView());

	return true;
}

void TestApplication::draw() {

	// clear the screen for this frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

#pragma region DrawCode

	m_grid->draw(m_camera);

	//m_fbxLoader->draw(m_camera);

#pragma endregion DrawCode
	
	// display the 3D gizmos
	Gizmos::draw(m_camera->getProjectionView());
	
	// get a orthographic projection matrix and draw 2D gizmos
	int width = 0, height = 0;
	glfwGetWindowSize(m_window, &width, &height);
	mat4 guiMatrix = glm::ortho<float>(0, 0, (float)width, (float)height);

	m_grid->draw(m_camera);
}

void TestApplication::getFrustumPlanes(const mat4& transform, vec4* planes)
{
	// right side
	planes[0] = vec4(transform[0][3] - transform[0][0],
					 transform[1][3] - transform[1][0],
					 transform[2][3] - transform[2][0],
					 transform[3][3] - transform[3][0]);

	// left side
	planes[1] = vec4(transform[0][3] + transform[0][0],
					 transform[1][3] + transform[1][0],
					 transform[2][3] + transform[2][0],
					 transform[3][3] + transform[3][0]);

	// top
	planes[2] = vec4(transform[0][3] + transform[0][1],
					 transform[1][3] + transform[1][1],
					 transform[2][3] + transform[2][1],
					 transform[3][3] + transform[3][1]);

	// bottom
	planes[3] = vec4(transform[0][3] + transform[0][1],
					 transform[1][3] + transform[1][1],
					 transform[2][3] + transform[2][1],
					 transform[3][3] + transform[3][1]);

	// far
	planes[4] = vec4(transform[0][3] + transform[0][2],
					 transform[1][3] + transform[1][2],
					 transform[2][3] + transform[2][2],
					 transform[3][3] + transform[3][2]);

	// near
	planes[5] = vec4(transform[0][3] + transform[0][2],
					 transform[1][3] + transform[1][2],
					 transform[2][3] + transform[2][2],
					 transform[3][3] + transform[3][2]);

	for (int i = 0; i < 6; i++)
	{
		float d = length(vec3(planes[i]));
		planes[i] /= d;
	}
}