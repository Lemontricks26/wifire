#ifndef PARTICLE_H
#define PARTICLE_H

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/vec3.hpp>
#include "gl_core_4_4.h"

class Camera;

using glm::vec3;
using glm::vec4;

struct Particle {
	vec3		position;
	vec3		velocity;
	vec4		colour;
	float		size;
	float		lifetime;
	float		lifespan;
};

struct ParticleVertex {
	vec4	position;
	vec4	colour;
};

class ParticleEmitter {
public:
	ParticleEmitter() : m_particles(nullptr),
		m_firstDead(0),
		m_maxParticles(0),
		m_position(10, 0, 0),
		m_vao(0), m_vbo(0),
		m_ibo(0),
		m_vertexData(nullptr) {
	};

	virtual ~ParticleEmitter() {
		delete[] m_particles;
		delete[] m_vertexData;
		glDeleteVertexArrays(1, &m_vao);
		glDeleteBuffers(1, &m_vbo);
		glDeleteBuffers(1, &m_ibo);
	};

	void particleShader();
	void update(glm::mat4 transform);

	void initalise(unsigned int a_maxParticles,
		unsigned int a_emitRate,
		float a_lifetimeMin,
		float a_lifetimeMax,
		float a_velocityMin,
		float a_velocityMax,
		float a_startSize,
		float a_endSize,
		const vec4& a_startColour,
		const vec4& a_endColour);

	void emit();
	void update(float a_deltaTime, const glm::mat4& a_cameraTransform);
	void draw(Camera* camera);
	void updateTransform(glm::mat4 transform);

protected:

	Particle*			m_particles;
	unsigned int		m_firstDead;
	unsigned int		m_maxParticles;
	unsigned int		m_programID;
	unsigned int		m_vao, m_vbo, m_ibo;
	ParticleVertex* m_vertexData;

	vec3		m_position;
	float		m_emitTimer;
	float		m_emitRate;
	float		m_lifespanMin;
	float		m_lifespanMax;
	float		m_velocityMin;
	float		m_velocityMax;
	float		m_startSize;
	float		m_endSize;
	vec4		m_startColour;
	vec4		m_endColour;
};
#endif