#include "Grid.h"
#include <iostream>
#include "gl_core_4_4.h"
#include <glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/vec3.hpp>
#include <stb_image.h>
#include "Camera.h"

bool CheckCompileStatus(GLuint shaderId)
{
	GLint result = GL_FALSE;
	int logLength = 0;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);
	if (result != GL_TRUE) {
		char* logBuffer = NULL;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logLength);
		logBuffer = new char[logLength];
		glGetShaderInfoLog(shaderId, logLength, NULL, logBuffer);
		std::cerr << "Grid Compile Error: " << logBuffer << std::endl;
		delete[] logBuffer;
		return false;
	}
	return true;
}

bool CheckLinkStatus(GLuint programId)
{

	GLint result = GL_FALSE;
	int logLength = 0;
	glGetProgramiv(programId, GL_LINK_STATUS, &result);
	if (result != GL_TRUE) {
		char* logBuffer = NULL;
		glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &logLength);
		logBuffer = new char[logLength];
		glGetProgramInfoLog(programId, logLength, NULL, logBuffer);
		std::cerr << "Grid Link Error: " << logBuffer << std::endl;
		delete[] logBuffer;
		return false;
	}
	return true;
}

void Grid::shader()
{
	const char* vsSource = "#version 410\n \
							layout(location=0) in vec4 position; \
							layout(location=1) in vec2 texcoord; \
							uniform mat4 view_proj; \
							out vec2 frag_texcoord; \
							uniform sampler2D perlin_texture; \
							void main() { \
							vec4 pos = position; \
							pos.y += texture(perlin_texture, texcoord).r * 5; \
							frag_texcoord = texcoord; \
							gl_Position = view_proj * pos; }";


	const char* fsSource = "#version 410\n \
							in vec2 frag_texcoord; \
							out vec4 out_color; \
							uniform sampler2D perlin_texture; \
							uniform sampler2D sand; \
							void main() { \
							out_color = texture(sand, frag_texcoord * 5); \
							out_color.a = 1; }";

	int success = GL_FALSE;
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, (const char**)&vsSource, 0);
	glCompileShader(vertexShader);

	glShaderSource(fragmentShader, 1, (const char**)&fsSource, 0);
	glCompileShader(fragmentShader);

	m_programID = glCreateProgram();
	glAttachShader(m_programID, vertexShader);
	glAttachShader(m_programID, fragmentShader);
	glLinkProgram(m_programID);

	glGetProgramiv(m_programID, GL_LINK_STATUS, &success);
	if (success == GL_FALSE) {
		int infoLogLength = 0;
		glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(m_programID, infoLogLength, 0, infoLog);
		printf("Testapplication.cpp\n");
		printf("error: Failed to link shader program!\n");
		printf("%s\n", infoLog);
		delete[] infoLog;
	}
	
	CheckLinkStatus(m_programID);
	CheckCompileStatus(vertexShader);
	CheckCompileStatus(fragmentShader);

	glDeleteShader(fragmentShader);
	glDeleteShader(vertexShader);
}

void Grid::draw(Camera* camera)
{
	glUseProgram(m_programID);

	int loc = glGetUniformLocation(m_programID, "view_proj");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &(camera->getProjectionView()[0][0]));

	loc = glGetUniformLocation(m_programID, "perlin_texture");
	glUniform1i(loc, 0);


	loc = glGetUniformLocation(m_programID, "sand");
	glUniform1i(loc, 1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_sand);

	glBindVertexArray(m_VAO);
	unsigned int indexCount = (m_uiRows - 1) * (m_uiCols - 1) * 6;
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
}

void Grid::generateGrid(unsigned int rows, unsigned int cols)
{
	shader();

	int dims = 200;
	float *perlinData = new float[dims * dims];
	float scale = (1.0f / dims) * 3;
	int octaves = 6;

	for (int x = 0; x < dims; x++)
	{
		for (int y = 0; y < dims; y++)
		{
			float amplitude = 4.f;
			float persistence = 0.3f;
			perlinData[y * dims + x] = 0;

			for (int o = 0; o < octaves; o++)
			{
				float frequency = powf(2, (float)o);
				float perlin_Sample = glm::perlin(glm::vec2((float)x, (float)y) * scale * frequency) * 0.5f + 0.5f;
				perlinData[y * dims + x] += perlin_Sample * amplitude;
				amplitude *= persistence;
			}
		}
	}

	int imageWidth = 0, imageHeight = 0, imageFormat = 0;
	unsigned char* Tex;

	Tex = stbi_load("./Model/sand.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);
	glGenTextures(1, &m_sand);
	glBindTexture(GL_TEXTURE_2D, m_sand);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, Tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	stbi_image_free(Tex);

	glGenTextures(1, &m_perlin_texture);
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, dims, dims, 0, GL_RED, GL_FLOAT, perlinData);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	Vertex* aoVertices = new Vertex[rows * cols];

	for (unsigned int r = 0; r < rows; ++r)
	{
		for (unsigned int c = 0; c < cols; ++c)
		{
			aoVertices[r * cols + c].position = glm::vec4((float)c - (rows/2), perlinData[r  * dims + c] - 30, (float)r - (cols/2), 1);

			glm::vec2 texcoords = glm::vec2((c / (float)(cols - 1)), (r / (float)(rows - 1)));
			aoVertices[r * cols + c].texCoords = texcoords;
		}
	}

	unsigned int* auiIndices = new unsigned int[(rows - 1) * (cols - 1) * 6];
	unsigned int index = 0;

	for (unsigned int r = 0; r < (rows - 1); ++r)
	{
		for (unsigned int c = 0; c < (cols - 1); ++c)
		{
			// triangle 1
			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);

			// triangle 2
			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			auiIndices[index++] = r * cols + (c + 1);
		}
	}

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glGenBuffers(1, &m_IBO);
	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(Vertex), aoVertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + offsetof(Vertex, texCoords));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec4)));

	m_uiRows = rows;
	m_uiCols = cols;

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] aoVertices;
	delete[] auiIndices;
}