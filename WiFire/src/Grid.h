#ifndef GRID_H
#define GRID_H

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/vec3.hpp>

class Camera;

class Grid
{
public:
	struct Vertex {
		glm::vec4 position;
		glm::vec2 texCoords;
	};

	Grid() {}
	~Grid() {}

	void shader();
	void draw(Camera* camera);
	void generateGrid(unsigned int rows, unsigned int cols);

	unsigned int m_programID;

	unsigned int m_uiRows;
	unsigned int m_uiCols;

	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_IBO;

	unsigned int m_perlin_texture;
	unsigned int m_sand;
};
#endif