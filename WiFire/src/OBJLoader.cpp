#include "OBJLoader.h"

//void OBJLoader::createOpenGLBuffers(std::vector<tinyobj::shape_t>& shapes)
//{
//	m_gl_info.resize(shapes.size());
//
//	for (unsigned int mesh_index = 0; mesh_index < shapes.size(); ++mesh_index)
//	{
//		glGenVertexArrays(1, &m_gl_info[mesh_index].m_VAO);
//		glGenBuffers(1, &m_gl_info[mesh_index].m_VBO);
//		glGenBuffers(1, &m_gl_info[mesh_index].m_IBO);
//		glBindVertexArray(m_gl_info[mesh_index].m_VAO);
//
//		unsigned int float_count = shapes[mesh_index].mesh.positions.size();
//		float_count += shapes[mesh_index].mesh.normals.size();
//		float_count += shapes[mesh_index].mesh.texcoords.size();
//
//		std::vector<float> vertex_data;
//		vertex_data.reserve(float_count);
//
//		vertex_data.insert(vertex_data.end(),
//			shapes[mesh_index].mesh.positions.begin(),
//			shapes[mesh_index].mesh.positions.end());
//
//		vertex_data.insert(vertex_data.end(),
//			shapes[mesh_index].mesh.normals.begin(),
//			shapes[mesh_index].mesh.normals.end());
//
//		m_gl_info[mesh_index].m_index_count = shapes[mesh_index].mesh.indices.size();
//
//		glBindBuffer(GL_ARRAY_BUFFER, m_gl_info[mesh_index].m_VBO);
//		glBufferData(GL_ARRAY_BUFFER,
//			vertex_data.size() * sizeof(float),
//			vertex_data.data(), GL_STATIC_DRAW);
//
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_gl_info[mesh_index].m_IBO);
//		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
//			shapes[mesh_index].mesh.indices.size() * sizeof(unsigned int),
//			shapes[mesh_index].mesh.indices.data(), GL_STATIC_DRAW);
//
//		glEnableVertexAttribArray(0); // position
//		glEnableVertexAttribArray(1); // normal data
//		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
//		glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0,
//			(void*)(sizeof(float)*shapes[mesh_index].mesh.positions.size()));
//
//		glBindVertexArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//	}
//}

//void OBJLoader::render(Camera* camera)
//{
//	glUseProgram(m_program_id);
//
//	int view_proj_uniform = glGetUniformLocation(m_program_id, "projection_view");
//
//	glUniformMatrix4fv(view_proj_uniform, 1, GL_FALSE, (float*)&camera.view_proj);
//
//	for (unsigned int i = 0; i < m_gl_info.size(); ++i)
//	{
//		glBindVertexArray(m_gl_info[i].m_VAO);
//		glDrawElements(GL_TRIANGLES, m_gl_info[i].m_index_count, GL_UNSIGNED_INT, 0);
//	}
//}

//void OBJLoader::shader()
//{
//	const char* vsSource = "#version 410\n \
//							layout(location=0) in vec4 Position; \
//							layout(location=1) in vec4 Normal; \
//							out vec4 vNormal; \
//							uniform mat4 ProjectionView; \
//							void main() { vNormal = Normal; \
//							gl_Position = ProjectionView*Position; }";
//	const char* fsSource = "#version 410\n \
//							in vec4 vNormal; \
//							out vec4 FragColor; \
//							void main() { \
//							FragColor = vec4(1,1,1,1); }";
//
//	int success = GL_FALSE;
//	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
//	glShaderSource(vertexShader, 1, (const char**)&vsSource, 0);
//	glCompileShader(vertexShader);
//
//	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
//	glShaderSource(fragmentShader, 1, (const char**)&fsSource, 0);
//	glCompileShader(fragmentShader);
//
//	m_program_id = glCreateProgram();
//	glAttachShader(m_program_id, vertexShader);
//	glAttachShader(m_program_id, fragmentShader);
//	glLinkProgram(m_program_id);
//
//	glGetProgramiv(m_program_id, GL_LINK_STATUS, &success);
//	if (success == GL_FALSE) {
//		int infoLogLength = 0;
//		glGetProgramiv(m_program_id, GL_INFO_LOG_LENGTH, &infoLogLength);
//		char* infoLog = new char[infoLogLength];
//
//		glGetProgramInfoLog(m_program_id, infoLogLength, 0, infoLog);
//		printf("error: Failed to link FBXLoader shader program!\n");
//		printf("%s\n", infoLog);
//		delete[] infoLog;
//	}
//
//	glDeleteShader(vertexShader);
//	glDeleteShader(fragmentShader);
//}